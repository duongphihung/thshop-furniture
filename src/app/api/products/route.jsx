import prisma from '../../lib/prisma';

import { NextResponse } from 'next/server';

export async function GET(req) {
    const products = await prisma.product.findMany();
    return NextResponse.json({
        message: "All List",
        products,
    }, { status: 200 });
}

export async function POST(req) {
    try {
        const body = await req.json();
        const data = body.formData;
        console.log(data);

        return NextResponse.json({
            message: "", data
        }, { status: 200 });
        // if (!data) {
        //     return NextResponse.json({
        //         message: "Error", error
        //     }, { status: 500 })
        // }
    } catch (error) {
        console.log(error);
        return NextResponse.json({
            message: "Error", error
        }, { status: 500 })
    }
}