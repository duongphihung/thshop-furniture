import { NextResponse } from "next/server";
export async function GET(req) {
    return NextResponse.json({ message: "This Worked", success: true });
}

export async function POST(req) {
    try {
        const formData = await req.formData();
        console.log(formData);

        const f = formData.get("files");
        console.log("File Upload", JSON.stringify(f));
        return NextResponse.json({ message: "This Worked", success: true });
    } catch (err) {
        console.log(err);
        return NextResponse.json({ message: err, success: false });
    }
}