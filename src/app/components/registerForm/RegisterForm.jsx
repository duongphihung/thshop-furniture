"use client"
import Link from "next/link"
import "./registerForm.css"
import { useRouter } from 'next/navigation';

const RegisterForm = () => {
    const router = useRouter();
    const handleSignUp = async (e) => {
        e.preventDefault();
        const formData = new FormData(e.currentTarget);
        const data = {};

        formData.forEach((value, key) => {
            data[key] = value;
        });

        try {
            const response = await fetch("http://192.168.1.6:3000/api/auth/signup", {
                method: "POST",
                headers: {
                    "Content-Type": "application/json",
                },
                body: JSON.stringify(data),
            });
            console.log(response);
        } catch (error) {
            console.error("An error occurred during the fetch operation:", error);
        }
    }
    return (
        <div className="register-container">
            <div className="register-title">Registration</div>
            <form onSubmit={handleSignUp}>
                <div className="register-details">
                    <div className="input-box">
                        <span className="details">Full Name</span>
                        <input type="text" placeholder="Enter your name" className="" />
                    </div>
                    <div className="input-box">
                        <span className="details">User Name</span>
                        <input type="text" placeholder="Enter your username to login" className="" />
                    </div>
                    <div className="input-box">
                        <span className="details">Email</span>
                        <input type="email" placeholder="Enter your email" name="email" required className="" />
                    </div>
                    <div className="input-box">
                        <span className="details">Phone Number</span>
                        <input type="text" placeholder="Enter your number" className="" />
                    </div>
                    <div className="input-box">
                        <span className="details">Password</span>
                        <input type="text" placeholder="Enter your password" name="password" required className="" />
                    </div>
                    <div className="input-box">
                        <span className="details">Confirm Password</span>
                        <input type="text" placeholder="Comfirm your password" name="comfirmPassword" required className="" />
                    </div>
                </div>

                <div className="register-btn">
                    <input type="submit" value="Register" />
                </div>

                <div className="text">
                    <span>Already have an account?</span>
                    <Link href="/login" className="link-login">
                        <span>Login now</span>
                    </Link>
                </div>
            </form>
        </div>
    )
}

export default RegisterForm