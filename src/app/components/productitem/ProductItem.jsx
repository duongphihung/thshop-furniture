import { AiFillStar, AiOutlineCamera, AiOutlineEye, AiOutlineHeart, AiOutlineShoppingCart, AiOutlineStar } from "react-icons/ai"
import "./productitem.css"
import Link from "next/link"

const ProductItem = ({ props }) => {
    const { id, name, img, des, price } = props;
    return (
        <div className="product-grid">
            <div className="showcase">
                <div className="showcase-banner">
                    
                    <img src={img} alt="chair1" className="product-img default" />
                    <img src={img} alt="chair2" className="product-img hover" />


                    <div className="showcase-actions">
                        <button className="btn-action">
                            <AiOutlineHeart className="product_btn-icon"/>
                        </button>
                        <button className="btn-action">
                            <Link href={`product/${id}`}><AiOutlineEye className="product_btn-icon"/></Link>
                        </button>
                        <button className="btn-action">
                            <AiOutlineShoppingCart className="product_btn-icon"/>
                        </button>
                        <button className="btn-action">
                            <Link href="https://playcanv.as/p/tXiVwuwF/"><AiOutlineCamera className="product_btn-icon" /></Link>
                        </button>
                    </div>
                </div>
                <div className="showcase-content">
                    <Link href="/" className="showcase-category">{name}</Link>

                    <Link href="/">
                        <h3 className="showcase-title">{des}</h3>
                    </Link>

                    <div className="showcase-rating">
                        <AiFillStar />
                        <AiFillStar />
                        <AiFillStar />
                        <AiFillStar />
                        <AiOutlineStar />
                    </div>

                    <div className="price-box">
                        <p className="price">{price}</p>
                        <del>$75.00</del>
                    </div>

                </div>
            </div>

        </div>
    )
}

export default ProductItem