const products = [
    {
        id: 1,
        name: "Sofa",
        des: "A comfortable and stylish sofa, perfect for relaxing evenings at home.",
        img: "/products/1.png",
        stock: 10,
        price: "$100"
    },
    {
        id: 2,
        name: "Good chair",
        des: "A sturdy and reliable chair that complements any room decor.",
        img: "/products/2.png",
        stock: 10,
        price: "$200"
    },
    {
        id: 3,
        name: "Chair Xr",
        des: "An ergonomic chair designed for maximum comfort during long hours of work.",
        img: "/products/3.png",
        stock: 10,
        price: "$120"
    },
    {
        id: 4,
        name: "Hose chair",
        des: "A modern chair with a unique design, adding elegance to your living space.",
        img: "/products/4.png",
        stock: 10,
        price: "$230"
    },
    {
        id: 5,
        name: "Malex Chair",
        des: "A luxurious chair crafted with premium materials for ultimate relaxation.",
        img: "/products/5.png",
        stock: 10,
        price: "$300"
    },
    {
        id: 6,
        name: "Convin chair",
        des: "A versatile chair suitable for various settings, blending style and functionality.",
        img: "/products/6.png",
        stock: 10,
        price: "$270"
    },
    {
        id: 7,
        name: "Ceramic chair",
        des: "A contemporary chair with a sleek design, perfect for modern interiors.",
        img: "/products/7.png",
        stock: 10,
        price: "$250"
    },
    {
        id: 8,
        name: "Vinfa chair",
        des: "An elegant chair exuding sophistication and comfort in equal measure.",
        img: "/products/8.png",
        stock: 10,
        price: "$320"
    },
]

export default products;