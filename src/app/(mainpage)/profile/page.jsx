"use client"
import Link from "next/link"
import "./profile.css"
import { LoadingOutlined, PlusOutlined } from '@ant-design/icons';
import { message, Upload } from 'antd';
import { useState } from "react";

import { TbUserEdit } from "react-icons/tb";
import { BsGear } from "react-icons/bs";
import { CiEdit } from "react-icons/ci";

const getBase64 = (img, callback) => {
    const reader = new FileReader();
    reader.addEventListener('load', () => callback(reader.result));
    reader.readAsDataURL(img);
};
    const beforeUpload = (file) => {
        const isJpgOrPng = file.type === 'image/jpeg' || file.type === 'image/png';
        if (!isJpgOrPng) {
        message.error('You can only upload JPG/PNG file!');
        }
        const isLt2M = file.size / 1024 / 1024 < 2;
        if (!isLt2M) {
        message.error('Image must smaller than 2MB!');
        }
        return isJpgOrPng && isLt2M;
};

const Profile = () => {
    const [loading, setLoading] = useState(false);
    const [imageUrl, setImageUrl] = useState();
    const handleChange = (info) => {
        if (info.file.status === 'uploading') {
        setLoading(true);
        return;
        }
        if (info.file.status === 'done') {
        // Get this url from response in real world.
        getBase64(info.file.originFileObj, (url) => {
            setLoading(false);
            setImageUrl(url);
        });
        }
    };
    const uploadButton = (
        <div>
        {loading ? <LoadingOutlined /> : <PlusOutlined />}
        <div
            style={{
            marginTop: 8,
            }}
        >
            Upload
        </div>
        </div>
    );
    return (
        <div className="profile-container">
            <div className="profile-sidebar">
                <div className="profile-sidebar-acc">
                    <img src="/avatar-user.jpg" alt="avatar" className="profile-acc-avatar" />
                    <p>Phii Hung</p>
                    <CiEdit className="icon-edit-profile"/>
                </div>
                <div className="profile-sidebar-lists">
                    <Link href="/profile"><TbUserEdit />Hồ sơ</Link>
                    <Link href="/profile/setting"><BsGear />Đổi mật khẩu</Link>
                </div>
            </div>
            <div className="profile-user-info">
                <h2>Hồ sơ của bạn</h2>
                <p>Quản lý hồ sơ của bạn</p>
                <div className="profile-user-container">
                    <div className="profile-user-table">
                        <table>
                            <tbody>
                                <tr>
                                    <td>Tên đăng nhập</td>
                                    <td>dphung1010</td>
                                </tr>
                                <tr>
                                    <td>Tên</td>
                                    <td>Dương Phi Hùng</td>
                                </tr>
                                <tr>
                                    <td>Số điện thoại</td>
                                    <td>0123 456 789</td>
                                    
                                </tr>
                                <tr>
                                    <td>Địa chỉ</td>
                                    <td>400 Doan Uan, Khue My, Ngu Hanh Son, Da Nang-VN</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <div className="profile-user-ava">
                        <Upload
                            name="avatar"
                            listType="picture-card"
                            className="avatar-uploader"
                            showUploadList={false}
                            action="https://run.mocky.io/v3/435e224c-44fb-4773-9faf-380c5e6a2188"
                            beforeUpload={beforeUpload}
                            onChange={handleChange}
                        >
                            {imageUrl ? (
                            <img
                                src={imageUrl}
                                alt="avatar"
                                style={{
                                width: '100%',
                                }}
                            />
                            ) : (
                            uploadButton
                            )}
                        </Upload>
                        <p>Dụng lượng file tối đa 1 MB</p>
                        <p>Định dạng: .JPEG, .JPG, .PNG</p>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Profile