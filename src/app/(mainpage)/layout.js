import ChatBot from "../components/chatbot/ChatBot";
import Footer from "../components/footer/Footer";
import NavBar from "../components/navbar/NavBar";
import "./layout.css";

export default async function RootLayout({ children }) {
    return (
        <div>
            <div className="container">
                <NavBar />
                <div className="wrapper">
                    {children}
                </div>
                <div className="chatbot"><ChatBot /></div>
                <Footer />
            </div>
        </div>
    )
}