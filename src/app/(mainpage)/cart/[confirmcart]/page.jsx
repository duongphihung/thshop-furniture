"use client"
import "./pageConfirmCart.css"
import React, { useEffect, useState } from 'react';
import { Input, Radio, Space, Select } from 'antd';

import { CSSTransition } from 'react-transition-group';

const ConfirmCart = () => {

    // function of antd
    const filterOption = (input, option) =>
        (option?.label ?? '').toLowerCase().includes(input.toLowerCase());
    
    const [value, setValue] = useState(1);
    const onChange = (e) => {
        console.log('radio checked', e.target.value);
        setValue(e.target.value);
    };

    // ToggleForm
    const [showForm, setShowForm] = useState(false);
    const toggleForm = () => {
        setShowForm(!showForm);
    };

    // Call API Address VN
    const [provinces, setProvinces] = useState([]);
    const [districts, setDistricts] = useState([]);
    const [wards, setWards] = useState([]);

    useEffect(() => {
        // Fetch provinces data
        fetch('https://provinces.open-api.vn/api/?depth=3')
            .then((response) => response.json())
            .then((data) => setProvinces(data));
    }, []);

    const handleProvinceChange = (value) => {
        // Fetch districts based on selected province
        const selectedProvince = provinces.find((province) => province.name === value);
        if (selectedProvince) {
            setDistricts(selectedProvince.districts);
        }
    };

    const handleDistrictChange = (value) => {
        // Fetch wards based on selected district
        const selectedDistrict = districts.find((district) => district.name === value);
        if (selectedDistrict) {
            setWards(selectedDistrict.wards);
        }
    };

    const [orderDetails] = useState({
        address: 'QL14, Hoa Nhon, Hoa Vang, DN',
        phoneNumber: '0123 456 789',
        productName: 'Tên sản phẩm',
        productImage: '/products/1.png',
        productPrice: '$100',
        quantity: 2,
        email: 'ntt.19it2@vku.udn.vn',
        contactPhone: '0987 654 321',
        totalAmount: 210,
        shippingFee: 10,
    });

    
    return (
        <div className="container-cart">
            <div className="left-panel">
                <div className="card">
                    <div className="card-address">
                        <h2>Delivery address</h2>
                        <button className="cartconfirm-edit" onClick={toggleForm}>Edit</button>
                    </div>
                    <p>Nguyễn Tấn Tài</p>
                    <p>Address: QL14, Hoa Nhon, Hoa Vang, DN</p>
                    <p>Phone number: 0123 456 789</p>
                </div>
                <div className="card">
                    <h2>Product information</h2>
                    <div className="card-prodInfo">
                        <img src="/products/1.png" alt="Product Image" className="card-prodInfo-img"/>
                        <p>Sofa</p>
                        <p>$100</p>
                        <p>Quantity: 1</p>
                    </div>
                    <div className="card-prodInfo">
                        <img src="/products/2.png" alt="Product Image" className="card-prodInfo-img"/>
                        <p>Good Chair</p>
                        <p>$200</p>
                        <p>Quantity: 1</p>
                    </div>
                    <div className="card-prodInfo">
                        <img src="/products/3.png" alt="Product Image" className="card-prodInfo-img"/>
                        <p>Chair XR</p>
                        <p>$120</p>
                        <p>Quantity: 1</p>
                    </div>
                </div>
            </div>

            {/* form address */}
            <CSSTransition in={showForm} classNames="form-overlay" timeout={300} >
                <div className={`overlay ${showForm ? 'active' : ''}`} onClick={toggleForm}></div>
            </CSSTransition>
            <CSSTransition in={showForm} classNames="menu" timeout={300} >
                <div className={`form-container ${showForm ? 'active' : ''}`}>
                    <form className="address-form">
                        <div className="addform-left-section">
                            <div className="addform-user-info">
                                <p>Họ tên</p>
                                <input
                                    type="text"
                                    placeholder="Họ và tên"
                                />
                            </div>
                            <div className="addform-user-info">
                                <p>Số điện thoại</p>
                                <input
                                    type="text"
                                    placeholder="Số điện thoại"
                                />
                            </div>
                        </div>

                        <div className="addform-right-section">
                            <div className="addform-user-info">
                                <p>Địa chỉ nhận hàng</p>
                                <input
                                    type="text"
                                    placeholder="Địa chỉ nhận hàng"
                                />
                            </div>
                            <div className="addform-user-info">
                                <p>Tỉnh/ Thành phố</p>
                                <Select
                                    showSearch
                                    defaultValue=""
                                    filterOption={filterOption}
                                    placeholder="Select province"
                                    onChange={handleProvinceChange}
                                    style={{ width: '100%', height: '37px' }}
                                    options={provinces.map((province) => ({
                                        value: province.name,
                                        label: province.name,
                                    }))}
                                />
                            </div>
                            <div className="addform-user-info">
                                <p>Quận/ Huyện</p>
                                <Select
                                    showSearch
                                    defaultValue=""
                                    filterOption={filterOption}
                                    placeholder="Select district"
                                    onChange={handleDistrictChange}
                                    style={{ width: '100%', height: '37px' }}
                                    options={districts.map((district) => ({
                                        value: district.name,
                                        label: district.name,
                                    }))}
                                />
                            </div>
                            <div className="addform-user-info">
                                <p>Phường/ Xã</p>
                                <Select
                                    showSearch
                                    defaultValue=""
                                    filterOption={filterOption}
                                    placeholder="Select ward"
                                    style={{ width: '100%', height: '37px' }}
                                    options={wards.map((ward) => ({
                                        value: ward.name,
                                        label: ward.name,
                                    }))}
                                />
                            </div>
                            <div className="buttons">
                                <button className="addform-btn-cancel" onClick={toggleForm}>Hủy</button>
                                <button className="addform-btn-save">Lưu</button>
                            </div>
                        </div>
                    </form>
                </div>
            </CSSTransition>


            <div className="right-panel">
                <div className="card">
                    <h2>Payment methods</h2>
                    <Radio.Group onChange={onChange} value={value}>
                        <Space direction="vertical">
                            <div className="payment-choice">
                                <Radio value={1}>
                                    <div className="payment-option">
                                        <img src="/payment/momo_logo.png" alt="img payment" className="payment-logo" />
                                        <p>Momo</p>
                                    </div>
                                </Radio>
                            </div>
                            <div className="payment-choice">
                                <Radio value={2}>
                                    <div className="payment-option">
                                        <img src="https://cdn.haitrieu.com/wp-content/uploads/2022/10/Logo-ZaloPay.png" alt="img payment" className="payment-logo" />
                                        <p>ZaloPay</p>
                                    </div>
                                </Radio>
                            </div>
                            <div className="payment-choice">
                                <Radio value={3}>
                                    <div className="payment-option">
                                        <img src="https://lzd-img-global.slatic.net/g/tps/tfs/O1CN012GqGXB23W2xdKEEa1_!!6000000007262-2-tps-284-200.png_2200x2200q75.png_.webp" alt="img payment" className="payment-logo" />
                                        <p>Local card</p>
                                    </div>
                                </Radio>
                            </div>
                        </Space>
                    </Radio.Group>
                    <h3>Contact Info</h3>
                    <p>Email: ntt.19it2@vku.udn.vn</p>
                    <p>Phone number: 0123 456 789</p>
                    <h3>Bill</h3>
                    <p>The number of products: 3</p>
                    <p>Price: 430</p>
                    <p>Transport fee: ${orderDetails.shippingFee}</p>
                    <p>The total amount: <span className="confirm-total-price">$430</span></p>
                    <button type="button">Order</button>
                </div>
            </div>
        </div>
    )
}

export default ConfirmCart