"use client"
import { useState } from "react";
import "./pageCart.css";
import { AiOutlineMinus, AiOutlinePlus } from "react-icons/ai";

const CartPage = () => {
    const [quantity, setQuantity] = useState(1);

    const handleMinusClick = () => {
        if (quantity > 1) {
            setQuantity(quantity - 1);
        }
    };

    const handlePlusClick = () => {
        setQuantity(parseInt(quantity, 10) + 1);
    };

    const handleInputChange = (event) => {
        const value = event.target.value;
        // Validate the input here if needed
        setQuantity(value);
    };

    return (
        <div className="cart-page-container cart-page">
            <table>
                <tbody>
                    <tr>
                        <th>Product</th>
                        <th>Quantity</th>
                        <th>Subtotal</th>
                    </tr>
                    <tr>
                        <td>
                            <div className="cart-info">
                                <img src="/products/1.png" alt="cart img" />
                                <div>
                                    <p>Sofa</p>
                                    <small>Price: $100</small>
                                    <button>Remove</button>
                                </div>
                            </div>
                        </td>
                        <td>
                            <div className="cart-page-quantity">
                                <button className="btn btn-quantity-minus" onClick={handleMinusClick}><AiOutlineMinus /></button>
                                <input type="text" value={quantity} onChange={handleInputChange}/>
                                <button className="btn btn-quantity-plus" onClick={handlePlusClick}><AiOutlinePlus /></button>
                            </div>
                        </td>
                        <td>$100</td>
                    </tr>
                    <tr>
                        <td>
                            <div className="cart-info">
                                <img src="/products/2.png" alt="cart img" />
                                <div>
                                    <p>Good Chair</p>
                                    <small>Price: $200</small>
                                    <button>Remove</button>
                                </div>
                            </div>
                        </td>
                        <td>
                            <div className="cart-page-quantity">
                                <button className="btn btn-quantity-minus" onClick={handleMinusClick}><AiOutlineMinus /></button>
                                <input type="text" value={quantity} onChange={handleInputChange}/>
                                <button className="btn btn-quantity-plus" onClick={handlePlusClick}><AiOutlinePlus /></button>
                            </div>
                        </td>
                        <td>$200</td>
                    </tr>
                    <tr>
                        <td>
                            <div className="cart-info">
                                <img src="/products/3.png" alt="cart img" />
                                <div>
                                    <p>Chair XR</p>
                                    <small>Price: $120</small>
                                    <button>Remove</button>
                                </div>
                            </div>
                        </td>
                        <td>
                            <div className="cart-page-quantity">
                                <button className="btn btn-quantity-minus" onClick={handleMinusClick}><AiOutlineMinus /></button>
                                <input type="text" value={quantity} onChange={handleInputChange}/>
                                <button className="btn btn-quantity-plus" onClick={handlePlusClick}><AiOutlinePlus /></button>
                            </div>
                        </td>
                        <td>$120</td>
                    </tr>
                </tbody>
            </table>

            <div className="cart-total-price">
                <table>
                    <tbody>
                        <tr>
                            <td>Subtotal</td>
                            <td>$420</td>
                        </tr><tr>
                            <td>Delivery</td>
                            <td>$30.00</td>
                        </tr><tr>
                            <td>Total</td>
                            <td>$430</td>
                        </tr>
                        <tr>
                            <th colSpan="2">Confirm Cart</th>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    )
}

export default CartPage;