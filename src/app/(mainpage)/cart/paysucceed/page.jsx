import Link from "next/link"
import "./paysucceed.css"

const PaySucceed = () => {
    return (
        <div className="paysucceed-container">
            <img src="/icon-delivery.jpg" alt="img" className="paysucceed-img" />
            <p>Your order is being processed</p>
            <p>Thanks for your order!</p>
            <Link href="/">Go to home</Link>
        </div>
    )
}

export default PaySucceed