import Link from "next/link"
import "./pageProduct.css"
import { PiFunnelLight } from "react-icons/pi";
import ProductItem from "../../components/productitem/ProductItem"
import Filter from "../../components/filter/Filter"
import { Button, Popover } from 'antd';

import products from "@/app/data/data";

const content = (
    <div>
        <Filter />
    </div>
);

const PageProduct = () => {
    return (
        <div className="page-product">

            <div className="icon-funnel-filter">
                <Popover
                    placement="rightTop"
                    content={content}
                    title="Filter"
                    trigger="click"
                >
                    <PiFunnelLight className="btn-icon-filter" />
                </Popover>
            </div>
            <div className="sidebar">
                <h1>Filter</h1>
                <div className="pro-filter">
                    <Filter />
                </div>
            </div>

            <div className="griddot">
                {
                    products.map(product => (
                        <ProductItem key={product.id} props={product} />
                    ))
                }
            </div>
        </div>
    )
}

export default PageProduct