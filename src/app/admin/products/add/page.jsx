"use client";
import { useRouter } from "next/navigation";
import React, { useState } from "react";
import {
    Card,
    CardContent,
    Divider,
    Box,
    Typography,
    TextField,
    FormControlLabel,
    Checkbox,
    Button,
    Grid,
    RadioGroup,
    Radio,
    FormControl,
    MenuItem,
    Input

} from "@mui/material";
import Stack from '@mui/material/Stack';
import AttachFileIcon from '@mui/icons-material/AttachFile';
import ImageList from '@mui/material/ImageList';
import ImageListItem from '@mui/material/ImageListItem';
import ChipsArray from "@/app/components/admin/ChipArray";
import FileInput from "@/app/components/fileinput/FileInput";
const numbers = [
    {
        value: "one",
        label: "Office chair",
    },
    {
        value: "two",
        label: "Regular chair",
    },
    {
        value: "three",
        label: "Furniture chairs",
    },
];

const initialFormData = {
    name: "",
    price: 0,
    description: "",
    categoryId: "Chair",
    sizes: "",
    quantity: "",
    color: "no",
    imageUrl: "",
    images: "",
};

const UserForm = () => {
    const router = useRouter();
    const [formData, setFormData] = useState(initialFormData);
    const [errorMessage, setErrorMessage] = useState("");

    

    const [number, setNumber] = React.useState("");

    const handleChangeCategory = (event) => {
        setNumber(event.target.value);
        console.log(event.target.value);
        setFormData({
            ...formData,
            categoryId: event.target.value,
        });
        console.log(formData);
    };

    const inputChange = (e) => {
        const value = e.target.value;
        const name = e.target.name;
        setFormData((prevState) => ({
            ...prevState,
            [name]: value,
        }));
    };

    const handleSubmit = async (e) => {
        e.preventDefault();
        setErrorMessage("");
        const res = await fetch("/api/products", {
            method: "POST",
            body: JSON.stringify({ formData }),
        });
        const data = await res.json();
        console.log(data);

        // if (!res.ok) {
        //     const response = await res.json();
        //     setErrorMessage(response.message);
        // } else {
        //     router.refresh();
        //     router.push("/");
        // }
    };

    return (
        <Grid container spacing={0}>
            <Grid item lg={12} md={12} xs={12}>
                <div>
                    {/* ------------------------------------------------------------------------------------------------ */}
                    {/* Basic Checkbox */}
                    {/* ------------------------------------------------------------------------------------------------ */}
                    <Card
                        variant="outlined"
                        sx={{
                            p: 0,
                        }}
                    >
                        <Box
                            sx={{
                                padding: "15px 30px",
                            }}
                            display="flex"
                            alignItems="center"
                        >
                            <Box flexGrow={1}>
                                <Typography
                                    sx={{
                                        fontSize: "18px",
                                        fontWeight: "500",
                                    }}
                                >
                                    Add product
                                </Typography>
                            </Box>
                        </Box>
                        <Divider />
                        <CardContent
                            sx={{
                                padding: "30px",
                            }}
                        >
                            <form>
                                <TextField
                                    id="default-value"
                                    label="Name product"
                                    variant="outlined"
                                    placeholder="Enter name product"
                                    name="name"
                                    onChange={inputChange}
                                    fullWidth
                                    sx={{
                                        mb: 2,
                                    }}
                                />
                                <TextField
                                    fullWidth
                                    id="standard-select-number"
                                    variant="outlined"
                                    name="categoryId"
                                    select
                                    placeholder="Choose category"
                                    label="Category"
                                    value={number}
                                    onChange={handleChangeCategory}
                                    sx={{
                                        mb: 2,
                                    }}
                                >
                                    {numbers.map((option) => (
                                        <MenuItem key={option.value} value={option.value}>
                                            {option.label}
                                        </MenuItem>
                                    ))}
                                </TextField>
                                <TextField
                                    prefix="$"
                                    id="cost-product-text"
                                    label="Cost product"
                                    name="price"
                                    type="number"
                                    variant="outlined"
                                    onChange={inputChange}
                                    fullWidth
                                    sx={{
                                        mb: 2,
                                    }}
                                />
                                <TextField
                                    id="color input"
                                    label="Color"
                                    name="color"
                                    type="text"
                                    variant="outlined"
                                    onChange={inputChange}
                                    fullWidth
                                    sx={{
                                        mb: 2,
                                    }}
                                />
                                <TextField
                                    id="quantity-input"
                                    label="Quantity"
                                    type="number"
                                    name="quantity"
                                    variant="outlined"
                                    onChange={inputChange}
                                    fullWidth
                                    sx={{
                                        mb: 2,
                                    }}
                                />
                                <TextField
                                    id="outlined-multiline-static"
                                    label="Description"
                                    name="description"
                                    placeholder="Enter description your product"
                                    onChange={inputChange}
                                    multiline
                                    rows={4}
                                    variant="outlined"
                                    fullWidth
                                    sx={{
                                        mb: 2,
                                    }}
                                />
                                <FileInput/>
                                <Stack
                                    flexDirection="row"
                                    justifyContent="center">
                                    <Button color="primary" onClick={handleSubmit} variant="contained">
                                        Add prodcut
                                    </Button>
                                </Stack>
                            </form>
                        </CardContent>
                    </Card>
                </div>
            </Grid>
        </Grid>
    );
};

export default UserForm;